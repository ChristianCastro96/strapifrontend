import './App.css';
import Home from './components/Home';
import Category from './components/Category';
import Review from './components/Review';
import Film from './components/Film';
import SiteHeader from './components/SiteHeader'
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import FilmCrud from './components/Crud/FilmCrud';
import Login from './components/Login';

//apollo client
const client = new ApolloClient({
  uri:"http://localhost:1337/graphql",
  cache:new InMemoryCache()
});


function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <SiteHeader/>
        <div className='container-fluid  text-danger h4'>
          <Switch>
            <Route exact path="/">
              <Home></Home>
            </Route>
            <Route path="/categories/:id">
              <Category ></Category>
            </Route>
            <Route path="/films/:id">
              <Film ></Film>
            </Route>
            <Route path="/reviews/:id">
              <Review ></Review>
            </Route>
            <Route path="/film/create">
              <FilmCrud ></FilmCrud>
            </Route>
            <Route path="/auth/login">
              <Login ></Login>
            </Route>
          </Switch>
        </div>

      </Router>
    </ApolloProvider>
  );
}

export default App;
