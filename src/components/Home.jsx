import React from 'react';
// import useFetch from './Hooks/useFetch'
import { useQuery, gql } from '@apollo/client';
import {Link} from 'react-router-dom';

const FILMS = gql`
  query GetFilms{
    films{
      data{
        id,
        attributes{
          title,
          description,
          category{
            data{
              attributes{
                name
              }
            }
          }
        }
      }
    }
  }
`

export const films = (films) => {
  console.log("films",films)
  return films
  .map(film=> 
    <div key={film.id} className="card col-12 col-md-4 col-lg-2 py-5 text-center text-dark mx-2">
      <h3 className="card-title ">{film.attributes.title}</h3>
      <p className="card-text h6 text-info">{film.attributes.category.data? film.attributes.category.data.attributes.name :"Nessuna categoria associata"}</p>
      <p className="card-text h6">{film.attributes.description.substring(0,65)+"... "}</p>
      <Link to={`/films/${film.id}`} className="btn btn-primary">Visualizza dettagli</Link>
    </div>     
  )
}

const Home = () => {
  
  // const {data,error,loading} = useFetch("http://localhost:1337/api/films");
  const {data,error,loading} = useQuery(FILMS);


  return (
    <>
    <div className='container'>
      <h1 className="text-warning text-center bg-dark">HomePage</h1>
      <div className="row justify-content-center">
        {error && <p className='text-danger text-center'>Errore nel caricamento!</p>}
        {loading && <p className='text-info text-center'>Caricamento in corso...</p>}
        {data && films(data.films.data) }
      </div>
    </div>
    </>
    
  )
}

export default Home