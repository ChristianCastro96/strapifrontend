import {useState,useEffect} from 'react'

const useFetch = (api) => {
    const [data,setData] = useState(null);
    const [loading,setLoading] = useState(true);
    const [error,setError] = useState(null);

    useEffect(() => {
        
        const fetchData  = async() => {

            try{
                const response = await fetch(api);
                const json = await response.json()
                
                setLoading(false);
                setData(json);

            }
            
            catch(error){
                setLoading(false);
                setError(error);
            }


        }

        fetchData();



    }, [api])
    
    return{error,loading,data}



}

export default useFetch