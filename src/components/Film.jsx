import React from 'react'
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
// import useFetch from './Hooks/useFetch'
import { useQuery, gql } from '@apollo/client';

const Film = () => {
  const {id} = useParams();
  // const {data,error,loading} = useFetch(`http://localhost:1337/api/films/${id}`);

  const DESCRIPTION=gql`
  query GetFilm($id: ID!){
    film(id:$id){
      data{
        attributes{
          title,
          description
        }
      }
    }
  }
  `
  const {data,error,loading} = useQuery(DESCRIPTION,{variables:{id:id}});

  return (
    <>
      <h2 className='text-warning text-center bg-dark'>{"Caratteristiche del film"}</h2>
      <div className="row container-fluid">
        <h3 className="text-center text-danger"></h3>
        <div className="col-12 d-flex justify-content-center">

        {error && <p>{error}</p>}

        {loading && <p className='text-info'>Caricamento in corso...</p>}
        {data &&
          <>
            <div className="h5 text-success">{"Trama:"}
              <p className='text-dark fs-6'>{data.film.data.attributes.description}</p>
            </div>
          </>
        }

        </div>
      </div>
    </>
  )
}

export default Film