import React from 'react';
import { useQuery, gql } from '@apollo/client';
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { films } from './Home';


const CATEGORY = gql`
query GetCategories($id: ID!){
    category(id:$id){
      data{
        attributes{
          name,
          description,
          films{
            data{
              id
              attributes{
                title,
                description,
                category{
                  data{
                    id
                    attributes{
                      name
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`

const Category = () => {
  const {id} = useParams();
  const {data,error,loading} = useQuery(CATEGORY,{variables:{id:id}});

  return (
    <>
      <h2 className='text-warning text-center bg-dark'>Film {data&& data?.category.data.attributes.name}</h2>
      <div className="row container-fluid">

        <div className="col-12 d-flex justify-content-center">
          {error && <p>{error}</p>}
          {loading && <p className='text-info'>Caricamento in corso...</p>}
        </div>

        <div className="row justify-content-center">
          {data &&  films(data.category.data.attributes.films.data) }
          {(data && data.category.data.attributes.films.data.length===0) && "Non sono stati trovati film per la categoria: "+data.category.data.attributes.name }
        </div>
      </div>
    </>
  )
}

export default Category