import React,{useState} from 'react'

const Login = () => {
    const [objForm,setObjForm] = useState({}) 


    const handleSubmit = () => {
        fetch("http://localhost:1337/api/auth/local/register", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            
            body: JSON.stringify({
                "username": objForm.username,
                "email": objForm.email,
                "password": objForm.password
            })
        })
        .then( (res) => { 
           console.log("res",res)
        });
    }

    return (
        <>
            <h2 className='text-danger text-center'>Registrati</h2>
            <div className="row container-fluid justify-content-center">
                <form className="col-12 col-lg-6 form d-flex justify-content-center">
                    <input type="text" className="input" placeholder='username'
                        onChange={(e)=>{
                            setObjForm({...objForm, username:e.target.value})
                        }}
                    />
                    <input type="text" className="input" placeholder='email'
                        onChange={(e)=>{
                            setObjForm({...objForm, email:e.target.value})
                        }}
                    />
                    <input type="password" className="input" placeholder='password'
                        onChange={(e)=>{
                            setObjForm({...objForm, password:e.target.value})
                        }}
                    />
                    <button onClick={(e)=>{e.preventDefault();handleSubmit()}}>{"REGISTRATI"}</button>
                </form>
            </div>
        </>
    )
}

export default Login