import React from 'react'
import { Link } from 'react-router-dom';
import { useQuery, gql } from '@apollo/client';


export default function SiteHeader() {

  const CATEGORIES = gql`
  query GetCategories{
    categories{
      data{
        id,
        attributes{
          name,
          description,
          films{
            data{
              attributes{
                title,
                description
              }
            }
          }
        }
      }
    }
  }
`

// const {data,error,loading} = useFetch("http://localhost:1337/api/films");
const {data,error,loading} = useQuery(CATEGORIES);




  return (
    <div className="container-fluid">
        <Link className="text-decoration-none" to="/">
          <h1 className="text-info text-center " > Strapi Guide</h1>
        </ Link>

        <Link className='text-decoration-none' to="/auth/login">
          <button className="btn-info">
            Login  
          </button>
        </Link>
     
        {error && <p className='text-danger text-center'>Errore nel caricamento!</p>}
        {loading && <p className='text-info text-center'>Caricamento in corso...</p>}

        <div className="row border px-2">

          <div className="col-12 col-lg-4 d-flex align-items-center">
            Filtra per:  
          </div>


          <div className="col-12 col-lg-4">
            <div className='row'>
              {data && data.categories.data.map(category=>(
                <Link key={category.id} className='col-2 col-lg-2 mx-2  text-decoration-none' to={`/categories/${category.id}`}>
                  <p  >{category.attributes.name}</p>
                </Link>
              ))}
            </div>
          </div>

          <div className="col-12 col-lg-4 d-flex justify-content-end align-items-center">
            <Link className="text-decoration-none" to="/film/create">
              <button className="btn btn-success">Aggiungi Film</button>
            </ Link>
          </div>
          
        </div>
      </div>
  )
}