import React,{useState} from 'react'


// example of login
//  mutation {
//   login(input: { identifier: "usergeneric@test.it", password: "userGeneric" }) {
//     jwt
//   }
// }

// { "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjQ2OTI1MTA0LCJleHAiOjE2NDk1MTcxMDR9.meXhTq845OQ2VZ_vWBuqfq4LkfVTQ7wy3vlydpOC9FQ"}
const FilmCrud = () => {
    const [objForm,setObjForm] = useState({}) 



    const handleSubmit =() =>{
        console.log("OBJ",objForm)
    }



  return (
  <>
    <h2 className='text-warning text-center bg-dark'>{"Aggiungi un film"}</h2>
    <div className="row container-fluid justify-content-center">
        <form className="col-12 col-lg-6 form d-flex justify-content-center">
            <input type="text" className="input" placeholder='titolo'
                onChange={(e)=>{
                    setObjForm({...objForm, title:e.target.value})
                }}
            />
            <input type="text" className="input" placeholder='descrizione'
                onChange={(e)=>{
                    setObjForm({...objForm, description:e.target.value})
                }}
            />
            <button onClick={(e)=>{e.preventDefault();handleSubmit()}}>Aggiungi</button>
        </form>
    </div>
    </>
)
}

export default FilmCrud